package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ImportFlag;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.internal.objectstore.OsObjectBuilder;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class org_wBHARATmeet_model_realms_BroadcastRealmProxy extends org.wBHARATmeet.model.realms.Broadcast
    implements RealmObjectProxy, org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface {

    static final class BroadcastColumnInfo extends ColumnInfo {
        long maxColumnIndexValue;
        long broadcastIdIndex;
        long createdByNumberIndex;
        long timestampIndex;
        long usersIndex;

        BroadcastColumnInfo(OsSchemaInfo schemaInfo) {
            super(4);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("Broadcast");
            this.broadcastIdIndex = addColumnDetails("broadcastId", "broadcastId", objectSchemaInfo);
            this.createdByNumberIndex = addColumnDetails("createdByNumber", "createdByNumber", objectSchemaInfo);
            this.timestampIndex = addColumnDetails("timestamp", "timestamp", objectSchemaInfo);
            this.usersIndex = addColumnDetails("users", "users", objectSchemaInfo);
            this.maxColumnIndexValue = objectSchemaInfo.getMaxColumnIndex();
        }

        BroadcastColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new BroadcastColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final BroadcastColumnInfo src = (BroadcastColumnInfo) rawSrc;
            final BroadcastColumnInfo dst = (BroadcastColumnInfo) rawDst;
            dst.broadcastIdIndex = src.broadcastIdIndex;
            dst.createdByNumberIndex = src.createdByNumberIndex;
            dst.timestampIndex = src.timestampIndex;
            dst.usersIndex = src.usersIndex;
            dst.maxColumnIndexValue = src.maxColumnIndexValue;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();

    private BroadcastColumnInfo columnInfo;
    private ProxyState<org.wBHARATmeet.model.realms.Broadcast> proxyState;
    private RealmList<org.wBHARATmeet.model.realms.User> usersRealmList;

    org_wBHARATmeet_model_realms_BroadcastRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (BroadcastColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<org.wBHARATmeet.model.realms.Broadcast>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$broadcastId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.broadcastIdIndex);
    }

    @Override
    public void realmSet$broadcastId(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'broadcastId' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$createdByNumber() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.createdByNumberIndex);
    }

    @Override
    public void realmSet$createdByNumber(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.createdByNumberIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.createdByNumberIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.createdByNumberIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.createdByNumberIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public long realmGet$timestamp() {
        proxyState.getRealm$realm().checkIfValid();
        return (long) proxyState.getRow$realm().getLong(columnInfo.timestampIndex);
    }

    @Override
    public void realmSet$timestamp(long value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.timestampIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.timestampIndex, value);
    }

    @Override
    public RealmList<org.wBHARATmeet.model.realms.User> realmGet$users() {
        proxyState.getRealm$realm().checkIfValid();
        // use the cached value if available
        if (usersRealmList != null) {
            return usersRealmList;
        } else {
            OsList osList = proxyState.getRow$realm().getModelList(columnInfo.usersIndex);
            usersRealmList = new RealmList<org.wBHARATmeet.model.realms.User>(org.wBHARATmeet.model.realms.User.class, osList, proxyState.getRealm$realm());
            return usersRealmList;
        }
    }

    @Override
    public void realmSet$users(RealmList<org.wBHARATmeet.model.realms.User> value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("users")) {
                return;
            }
            // if the list contains unmanaged RealmObjects, convert them to managed.
            if (value != null && !value.isManaged()) {
                final Realm realm = (Realm) proxyState.getRealm$realm();
                final RealmList<org.wBHARATmeet.model.realms.User> original = value;
                value = new RealmList<org.wBHARATmeet.model.realms.User>();
                for (org.wBHARATmeet.model.realms.User item : original) {
                    if (item == null || RealmObject.isManaged(item)) {
                        value.add(item);
                    } else {
                        value.add(realm.copyToRealm(item));
                    }
                }
            }
        }

        proxyState.getRealm$realm().checkIfValid();
        OsList osList = proxyState.getRow$realm().getModelList(columnInfo.usersIndex);
        // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
        if (value != null && value.size() == osList.size()) {
            int objects = value.size();
            for (int i = 0; i < objects; i++) {
                org.wBHARATmeet.model.realms.User linkedObject = value.get(i);
                proxyState.checkValidObject(linkedObject);
                osList.setRow(i, ((RealmObjectProxy) linkedObject).realmGet$proxyState().getRow$realm().getIndex());
            }
        } else {
            osList.removeAll();
            if (value == null) {
                return;
            }
            int objects = value.size();
            for (int i = 0; i < objects; i++) {
                org.wBHARATmeet.model.realms.User linkedObject = value.get(i);
                proxyState.checkValidObject(linkedObject);
                osList.addRow(((RealmObjectProxy) linkedObject).realmGet$proxyState().getRow$realm().getIndex());
            }
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("Broadcast", 4, 0);
        builder.addPersistedProperty("broadcastId", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("createdByNumber", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("timestamp", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedLinkProperty("users", RealmFieldType.LIST, "User");
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static BroadcastColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new BroadcastColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "Broadcast";
    }

    public static final class ClassNameHelper {
        public static final String INTERNAL_CLASS_NAME = "Broadcast";
    }

    @SuppressWarnings("cast")
    public static org.wBHARATmeet.model.realms.Broadcast createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = new ArrayList<String>(1);
        org.wBHARATmeet.model.realms.Broadcast obj = null;
        if (update) {
            Table table = realm.getTable(org.wBHARATmeet.model.realms.Broadcast.class);
            BroadcastColumnInfo columnInfo = (BroadcastColumnInfo) realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.Broadcast.class);
            long pkColumnIndex = columnInfo.broadcastIdIndex;
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("broadcastId")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("broadcastId"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.Broadcast.class), false, Collections.<String> emptyList());
                    obj = new io.realm.org_wBHARATmeet_model_realms_BroadcastRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("users")) {
                excludeFields.add("users");
            }
            if (json.has("broadcastId")) {
                if (json.isNull("broadcastId")) {
                    obj = (io.realm.org_wBHARATmeet_model_realms_BroadcastRealmProxy) realm.createObjectInternal(org.wBHARATmeet.model.realms.Broadcast.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.org_wBHARATmeet_model_realms_BroadcastRealmProxy) realm.createObjectInternal(org.wBHARATmeet.model.realms.Broadcast.class, json.getString("broadcastId"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'broadcastId'.");
            }
        }

        final org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface objProxy = (org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) obj;
        if (json.has("createdByNumber")) {
            if (json.isNull("createdByNumber")) {
                objProxy.realmSet$createdByNumber(null);
            } else {
                objProxy.realmSet$createdByNumber((String) json.getString("createdByNumber"));
            }
        }
        if (json.has("timestamp")) {
            if (json.isNull("timestamp")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'timestamp' to null.");
            } else {
                objProxy.realmSet$timestamp((long) json.getLong("timestamp"));
            }
        }
        if (json.has("users")) {
            if (json.isNull("users")) {
                objProxy.realmSet$users(null);
            } else {
                objProxy.realmGet$users().clear();
                JSONArray array = json.getJSONArray("users");
                for (int i = 0; i < array.length(); i++) {
                    org.wBHARATmeet.model.realms.User item = org_wBHARATmeet_model_realms_UserRealmProxy.createOrUpdateUsingJsonObject(realm, array.getJSONObject(i), update);
                    objProxy.realmGet$users().add(item);
                }
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static org.wBHARATmeet.model.realms.Broadcast createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final org.wBHARATmeet.model.realms.Broadcast obj = new org.wBHARATmeet.model.realms.Broadcast();
        final org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface objProxy = (org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("broadcastId")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$broadcastId((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$broadcastId(null);
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("createdByNumber")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$createdByNumber((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$createdByNumber(null);
                }
            } else if (name.equals("timestamp")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$timestamp((long) reader.nextLong());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'timestamp' to null.");
                }
            } else if (name.equals("users")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$users(null);
                } else {
                    objProxy.realmSet$users(new RealmList<org.wBHARATmeet.model.realms.User>());
                    reader.beginArray();
                    while (reader.hasNext()) {
                        org.wBHARATmeet.model.realms.User item = org_wBHARATmeet_model_realms_UserRealmProxy.createUsingJsonStream(realm, reader);
                        objProxy.realmGet$users().add(item);
                    }
                    reader.endArray();
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'broadcastId'.");
        }
        return realm.copyToRealm(obj);
    }

    private static org_wBHARATmeet_model_realms_BroadcastRealmProxy newProxyInstance(BaseRealm realm, Row row) {
        // Ignore default values to avoid creating unexpected objects from RealmModel/RealmList fields
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        objectContext.set(realm, row, realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.Broadcast.class), false, Collections.<String>emptyList());
        io.realm.org_wBHARATmeet_model_realms_BroadcastRealmProxy obj = new io.realm.org_wBHARATmeet_model_realms_BroadcastRealmProxy();
        objectContext.clear();
        return obj;
    }

    public static org.wBHARATmeet.model.realms.Broadcast copyOrUpdate(Realm realm, BroadcastColumnInfo columnInfo, org.wBHARATmeet.model.realms.Broadcast object, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (org.wBHARATmeet.model.realms.Broadcast) cachedRealmObject;
        }

        org.wBHARATmeet.model.realms.Broadcast realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(org.wBHARATmeet.model.realms.Broadcast.class);
            long pkColumnIndex = columnInfo.broadcastIdIndex;
            String value = ((org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) object).realmGet$broadcastId();
            long rowIndex = Table.NO_MATCH;
            if (value == null) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, value);
            }
            if (rowIndex == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), columnInfo, false, Collections.<String> emptyList());
                    realmObject = new io.realm.org_wBHARATmeet_model_realms_BroadcastRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, columnInfo, realmObject, object, cache, flags) : copy(realm, columnInfo, object, update, cache, flags);
    }

    public static org.wBHARATmeet.model.realms.Broadcast copy(Realm realm, BroadcastColumnInfo columnInfo, org.wBHARATmeet.model.realms.Broadcast newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (org.wBHARATmeet.model.realms.Broadcast) cachedRealmObject;
        }

        org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface realmObjectSource = (org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) newObject;

        Table table = realm.getTable(org.wBHARATmeet.model.realms.Broadcast.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, columnInfo.maxColumnIndexValue, flags);

        // Add all non-"object reference" fields
        builder.addString(columnInfo.broadcastIdIndex, realmObjectSource.realmGet$broadcastId());
        builder.addString(columnInfo.createdByNumberIndex, realmObjectSource.realmGet$createdByNumber());
        builder.addInteger(columnInfo.timestampIndex, realmObjectSource.realmGet$timestamp());

        // Create the underlying object and cache it before setting any object/objectlist references
        // This will allow us to break any circular dependencies by using the object cache.
        Row row = builder.createNewObject();
        io.realm.org_wBHARATmeet_model_realms_BroadcastRealmProxy realmObjectCopy = newProxyInstance(realm, row);
        cache.put(newObject, realmObjectCopy);

        // Finally add all fields that reference other Realm Objects, either directly or through a list
        RealmList<org.wBHARATmeet.model.realms.User> usersList = realmObjectSource.realmGet$users();
        if (usersList != null) {
            RealmList<org.wBHARATmeet.model.realms.User> usersRealmList = realmObjectCopy.realmGet$users();
            usersRealmList.clear();
            for (int i = 0; i < usersList.size(); i++) {
                org.wBHARATmeet.model.realms.User usersItem = usersList.get(i);
                org.wBHARATmeet.model.realms.User cacheusers = (org.wBHARATmeet.model.realms.User) cache.get(usersItem);
                if (cacheusers != null) {
                    usersRealmList.add(cacheusers);
                } else {
                    usersRealmList.add(org_wBHARATmeet_model_realms_UserRealmProxy.copyOrUpdate(realm, (org_wBHARATmeet_model_realms_UserRealmProxy.UserColumnInfo) realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.User.class), usersItem, update, cache, flags));
                }
            }
        }

        return realmObjectCopy;
    }

    public static long insert(Realm realm, org.wBHARATmeet.model.realms.Broadcast object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(org.wBHARATmeet.model.realms.Broadcast.class);
        long tableNativePtr = table.getNativePtr();
        BroadcastColumnInfo columnInfo = (BroadcastColumnInfo) realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.Broadcast.class);
        long pkColumnIndex = columnInfo.broadcastIdIndex;
        String primaryKeyValue = ((org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) object).realmGet$broadcastId();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$createdByNumber = ((org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) object).realmGet$createdByNumber();
        if (realmGet$createdByNumber != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.createdByNumberIndex, rowIndex, realmGet$createdByNumber, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.timestampIndex, rowIndex, ((org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) object).realmGet$timestamp(), false);

        RealmList<org.wBHARATmeet.model.realms.User> usersList = ((org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) object).realmGet$users();
        if (usersList != null) {
            OsList usersOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.usersIndex);
            for (org.wBHARATmeet.model.realms.User usersItem : usersList) {
                Long cacheItemIndexusers = cache.get(usersItem);
                if (cacheItemIndexusers == null) {
                    cacheItemIndexusers = org_wBHARATmeet_model_realms_UserRealmProxy.insert(realm, usersItem, cache);
                }
                usersOsList.addRow(cacheItemIndexusers);
            }
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(org.wBHARATmeet.model.realms.Broadcast.class);
        long tableNativePtr = table.getNativePtr();
        BroadcastColumnInfo columnInfo = (BroadcastColumnInfo) realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.Broadcast.class);
        long pkColumnIndex = columnInfo.broadcastIdIndex;
        org.wBHARATmeet.model.realms.Broadcast object = null;
        while (objects.hasNext()) {
            object = (org.wBHARATmeet.model.realms.Broadcast) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            String primaryKeyValue = ((org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) object).realmGet$broadcastId();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$createdByNumber = ((org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) object).realmGet$createdByNumber();
            if (realmGet$createdByNumber != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.createdByNumberIndex, rowIndex, realmGet$createdByNumber, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.timestampIndex, rowIndex, ((org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) object).realmGet$timestamp(), false);

            RealmList<org.wBHARATmeet.model.realms.User> usersList = ((org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) object).realmGet$users();
            if (usersList != null) {
                OsList usersOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.usersIndex);
                for (org.wBHARATmeet.model.realms.User usersItem : usersList) {
                    Long cacheItemIndexusers = cache.get(usersItem);
                    if (cacheItemIndexusers == null) {
                        cacheItemIndexusers = org_wBHARATmeet_model_realms_UserRealmProxy.insert(realm, usersItem, cache);
                    }
                    usersOsList.addRow(cacheItemIndexusers);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, org.wBHARATmeet.model.realms.Broadcast object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(org.wBHARATmeet.model.realms.Broadcast.class);
        long tableNativePtr = table.getNativePtr();
        BroadcastColumnInfo columnInfo = (BroadcastColumnInfo) realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.Broadcast.class);
        long pkColumnIndex = columnInfo.broadcastIdIndex;
        String primaryKeyValue = ((org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) object).realmGet$broadcastId();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$createdByNumber = ((org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) object).realmGet$createdByNumber();
        if (realmGet$createdByNumber != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.createdByNumberIndex, rowIndex, realmGet$createdByNumber, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.createdByNumberIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.timestampIndex, rowIndex, ((org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) object).realmGet$timestamp(), false);

        OsList usersOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.usersIndex);
        RealmList<org.wBHARATmeet.model.realms.User> usersList = ((org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) object).realmGet$users();
        if (usersList != null && usersList.size() == usersOsList.size()) {
            // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
            int objects = usersList.size();
            for (int i = 0; i < objects; i++) {
                org.wBHARATmeet.model.realms.User usersItem = usersList.get(i);
                Long cacheItemIndexusers = cache.get(usersItem);
                if (cacheItemIndexusers == null) {
                    cacheItemIndexusers = org_wBHARATmeet_model_realms_UserRealmProxy.insertOrUpdate(realm, usersItem, cache);
                }
                usersOsList.setRow(i, cacheItemIndexusers);
            }
        } else {
            usersOsList.removeAll();
            if (usersList != null) {
                for (org.wBHARATmeet.model.realms.User usersItem : usersList) {
                    Long cacheItemIndexusers = cache.get(usersItem);
                    if (cacheItemIndexusers == null) {
                        cacheItemIndexusers = org_wBHARATmeet_model_realms_UserRealmProxy.insertOrUpdate(realm, usersItem, cache);
                    }
                    usersOsList.addRow(cacheItemIndexusers);
                }
            }
        }

        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(org.wBHARATmeet.model.realms.Broadcast.class);
        long tableNativePtr = table.getNativePtr();
        BroadcastColumnInfo columnInfo = (BroadcastColumnInfo) realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.Broadcast.class);
        long pkColumnIndex = columnInfo.broadcastIdIndex;
        org.wBHARATmeet.model.realms.Broadcast object = null;
        while (objects.hasNext()) {
            object = (org.wBHARATmeet.model.realms.Broadcast) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            String primaryKeyValue = ((org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) object).realmGet$broadcastId();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$createdByNumber = ((org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) object).realmGet$createdByNumber();
            if (realmGet$createdByNumber != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.createdByNumberIndex, rowIndex, realmGet$createdByNumber, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.createdByNumberIndex, rowIndex, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.timestampIndex, rowIndex, ((org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) object).realmGet$timestamp(), false);

            OsList usersOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.usersIndex);
            RealmList<org.wBHARATmeet.model.realms.User> usersList = ((org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) object).realmGet$users();
            if (usersList != null && usersList.size() == usersOsList.size()) {
                // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
                int objectCount = usersList.size();
                for (int i = 0; i < objectCount; i++) {
                    org.wBHARATmeet.model.realms.User usersItem = usersList.get(i);
                    Long cacheItemIndexusers = cache.get(usersItem);
                    if (cacheItemIndexusers == null) {
                        cacheItemIndexusers = org_wBHARATmeet_model_realms_UserRealmProxy.insertOrUpdate(realm, usersItem, cache);
                    }
                    usersOsList.setRow(i, cacheItemIndexusers);
                }
            } else {
                usersOsList.removeAll();
                if (usersList != null) {
                    for (org.wBHARATmeet.model.realms.User usersItem : usersList) {
                        Long cacheItemIndexusers = cache.get(usersItem);
                        if (cacheItemIndexusers == null) {
                            cacheItemIndexusers = org_wBHARATmeet_model_realms_UserRealmProxy.insertOrUpdate(realm, usersItem, cache);
                        }
                        usersOsList.addRow(cacheItemIndexusers);
                    }
                }
            }

        }
    }

    public static org.wBHARATmeet.model.realms.Broadcast createDetachedCopy(org.wBHARATmeet.model.realms.Broadcast realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        org.wBHARATmeet.model.realms.Broadcast unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new org.wBHARATmeet.model.realms.Broadcast();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (org.wBHARATmeet.model.realms.Broadcast) cachedObject.object;
            }
            unmanagedObject = (org.wBHARATmeet.model.realms.Broadcast) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface unmanagedCopy = (org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) unmanagedObject;
        org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface realmSource = (org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$broadcastId(realmSource.realmGet$broadcastId());
        unmanagedCopy.realmSet$createdByNumber(realmSource.realmGet$createdByNumber());
        unmanagedCopy.realmSet$timestamp(realmSource.realmGet$timestamp());

        // Deep copy of users
        if (currentDepth == maxDepth) {
            unmanagedCopy.realmSet$users(null);
        } else {
            RealmList<org.wBHARATmeet.model.realms.User> managedusersList = realmSource.realmGet$users();
            RealmList<org.wBHARATmeet.model.realms.User> unmanagedusersList = new RealmList<org.wBHARATmeet.model.realms.User>();
            unmanagedCopy.realmSet$users(unmanagedusersList);
            int nextDepth = currentDepth + 1;
            int size = managedusersList.size();
            for (int i = 0; i < size; i++) {
                org.wBHARATmeet.model.realms.User item = org_wBHARATmeet_model_realms_UserRealmProxy.createDetachedCopy(managedusersList.get(i), nextDepth, maxDepth, cache);
                unmanagedusersList.add(item);
            }
        }

        return unmanagedObject;
    }

    static org.wBHARATmeet.model.realms.Broadcast update(Realm realm, BroadcastColumnInfo columnInfo, org.wBHARATmeet.model.realms.Broadcast realmObject, org.wBHARATmeet.model.realms.Broadcast newObject, Map<RealmModel, RealmObjectProxy> cache, Set<ImportFlag> flags) {
        org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface realmObjectTarget = (org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) realmObject;
        org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface realmObjectSource = (org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface) newObject;
        Table table = realm.getTable(org.wBHARATmeet.model.realms.Broadcast.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, columnInfo.maxColumnIndexValue, flags);
        builder.addString(columnInfo.broadcastIdIndex, realmObjectSource.realmGet$broadcastId());
        builder.addString(columnInfo.createdByNumberIndex, realmObjectSource.realmGet$createdByNumber());
        builder.addInteger(columnInfo.timestampIndex, realmObjectSource.realmGet$timestamp());

        RealmList<org.wBHARATmeet.model.realms.User> usersList = realmObjectSource.realmGet$users();
        if (usersList != null) {
            RealmList<org.wBHARATmeet.model.realms.User> usersManagedCopy = new RealmList<org.wBHARATmeet.model.realms.User>();
            for (int i = 0; i < usersList.size(); i++) {
                org.wBHARATmeet.model.realms.User usersItem = usersList.get(i);
                org.wBHARATmeet.model.realms.User cacheusers = (org.wBHARATmeet.model.realms.User) cache.get(usersItem);
                if (cacheusers != null) {
                    usersManagedCopy.add(cacheusers);
                } else {
                    usersManagedCopy.add(org_wBHARATmeet_model_realms_UserRealmProxy.copyOrUpdate(realm, (org_wBHARATmeet_model_realms_UserRealmProxy.UserColumnInfo) realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.User.class), usersItem, true, cache, flags));
                }
            }
            builder.addObjectList(columnInfo.usersIndex, usersManagedCopy);
        } else {
            builder.addObjectList(columnInfo.usersIndex, new RealmList<org.wBHARATmeet.model.realms.User>());
        }

        builder.updateExistingObject();
        return realmObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("Broadcast = proxy[");
        stringBuilder.append("{broadcastId:");
        stringBuilder.append(realmGet$broadcastId() != null ? realmGet$broadcastId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{createdByNumber:");
        stringBuilder.append(realmGet$createdByNumber() != null ? realmGet$createdByNumber() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{timestamp:");
        stringBuilder.append(realmGet$timestamp());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{users:");
        stringBuilder.append("RealmList<User>[").append(realmGet$users().size()).append("]");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        org_wBHARATmeet_model_realms_BroadcastRealmProxy aBroadcast = (org_wBHARATmeet_model_realms_BroadcastRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aBroadcast.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aBroadcast.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aBroadcast.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}
