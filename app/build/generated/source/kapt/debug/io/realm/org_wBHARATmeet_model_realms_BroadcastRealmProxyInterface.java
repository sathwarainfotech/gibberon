package io.realm;


public interface org_wBHARATmeet_model_realms_BroadcastRealmProxyInterface {
    public String realmGet$broadcastId();
    public void realmSet$broadcastId(String value);
    public String realmGet$createdByNumber();
    public void realmSet$createdByNumber(String value);
    public long realmGet$timestamp();
    public void realmSet$timestamp(long value);
    public RealmList<org.wBHARATmeet.model.realms.User> realmGet$users();
    public void realmSet$users(RealmList<org.wBHARATmeet.model.realms.User> value);
}
