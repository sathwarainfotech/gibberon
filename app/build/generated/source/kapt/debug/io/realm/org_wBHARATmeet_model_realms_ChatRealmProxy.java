package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ImportFlag;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.internal.objectstore.OsObjectBuilder;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class org_wBHARATmeet_model_realms_ChatRealmProxy extends org.wBHARATmeet.model.realms.Chat
    implements RealmObjectProxy, org_wBHARATmeet_model_realms_ChatRealmProxyInterface {

    static final class ChatColumnInfo extends ColumnInfo {
        long maxColumnIndexValue;
        long chatIdIndex;
        long lastMessageIndex;
        long lastMessageTimestampIndex;
        long userIndex;
        long isMutedIndex;
        long unReadCountIndex;
        long firstUnreadMessageIdIndex;
        long notificationIdIndex;
        long unreadMessagesIndex;

        ChatColumnInfo(OsSchemaInfo schemaInfo) {
            super(9);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("Chat");
            this.chatIdIndex = addColumnDetails("chatId", "chatId", objectSchemaInfo);
            this.lastMessageIndex = addColumnDetails("lastMessage", "lastMessage", objectSchemaInfo);
            this.lastMessageTimestampIndex = addColumnDetails("lastMessageTimestamp", "lastMessageTimestamp", objectSchemaInfo);
            this.userIndex = addColumnDetails("user", "user", objectSchemaInfo);
            this.isMutedIndex = addColumnDetails("isMuted", "isMuted", objectSchemaInfo);
            this.unReadCountIndex = addColumnDetails("unReadCount", "unReadCount", objectSchemaInfo);
            this.firstUnreadMessageIdIndex = addColumnDetails("firstUnreadMessageId", "firstUnreadMessageId", objectSchemaInfo);
            this.notificationIdIndex = addColumnDetails("notificationId", "notificationId", objectSchemaInfo);
            this.unreadMessagesIndex = addColumnDetails("unreadMessages", "unreadMessages", objectSchemaInfo);
            this.maxColumnIndexValue = objectSchemaInfo.getMaxColumnIndex();
        }

        ChatColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new ChatColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final ChatColumnInfo src = (ChatColumnInfo) rawSrc;
            final ChatColumnInfo dst = (ChatColumnInfo) rawDst;
            dst.chatIdIndex = src.chatIdIndex;
            dst.lastMessageIndex = src.lastMessageIndex;
            dst.lastMessageTimestampIndex = src.lastMessageTimestampIndex;
            dst.userIndex = src.userIndex;
            dst.isMutedIndex = src.isMutedIndex;
            dst.unReadCountIndex = src.unReadCountIndex;
            dst.firstUnreadMessageIdIndex = src.firstUnreadMessageIdIndex;
            dst.notificationIdIndex = src.notificationIdIndex;
            dst.unreadMessagesIndex = src.unreadMessagesIndex;
            dst.maxColumnIndexValue = src.maxColumnIndexValue;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();

    private ChatColumnInfo columnInfo;
    private ProxyState<org.wBHARATmeet.model.realms.Chat> proxyState;
    private RealmList<org.wBHARATmeet.model.realms.Message> unreadMessagesRealmList;

    org_wBHARATmeet_model_realms_ChatRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (ChatColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<org.wBHARATmeet.model.realms.Chat>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$chatId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.chatIdIndex);
    }

    @Override
    public void realmSet$chatId(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'chatId' cannot be changed after object was created.");
    }

    @Override
    public org.wBHARATmeet.model.realms.Message realmGet$lastMessage() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNullLink(columnInfo.lastMessageIndex)) {
            return null;
        }
        return proxyState.getRealm$realm().get(org.wBHARATmeet.model.realms.Message.class, proxyState.getRow$realm().getLink(columnInfo.lastMessageIndex), false, Collections.<String>emptyList());
    }

    @Override
    public void realmSet$lastMessage(org.wBHARATmeet.model.realms.Message value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("lastMessage")) {
                return;
            }
            if (value != null && !RealmObject.isManaged(value)) {
                value = ((Realm) proxyState.getRealm$realm()).copyToRealm(value);
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                // Table#nullifyLink() does not support default value. Just using Row.
                row.nullifyLink(columnInfo.lastMessageIndex);
                return;
            }
            proxyState.checkValidObject(value);
            row.getTable().setLink(columnInfo.lastMessageIndex, row.getIndex(), ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getIndex(), true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().nullifyLink(columnInfo.lastMessageIndex);
            return;
        }
        proxyState.checkValidObject(value);
        proxyState.getRow$realm().setLink(columnInfo.lastMessageIndex, ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getIndex());
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$lastMessageTimestamp() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.lastMessageTimestampIndex);
    }

    @Override
    public void realmSet$lastMessageTimestamp(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.lastMessageTimestampIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.lastMessageTimestampIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.lastMessageTimestampIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.lastMessageTimestampIndex, value);
    }

    @Override
    public org.wBHARATmeet.model.realms.User realmGet$user() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNullLink(columnInfo.userIndex)) {
            return null;
        }
        return proxyState.getRealm$realm().get(org.wBHARATmeet.model.realms.User.class, proxyState.getRow$realm().getLink(columnInfo.userIndex), false, Collections.<String>emptyList());
    }

    @Override
    public void realmSet$user(org.wBHARATmeet.model.realms.User value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("user")) {
                return;
            }
            if (value != null && !RealmObject.isManaged(value)) {
                value = ((Realm) proxyState.getRealm$realm()).copyToRealm(value);
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                // Table#nullifyLink() does not support default value. Just using Row.
                row.nullifyLink(columnInfo.userIndex);
                return;
            }
            proxyState.checkValidObject(value);
            row.getTable().setLink(columnInfo.userIndex, row.getIndex(), ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getIndex(), true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().nullifyLink(columnInfo.userIndex);
            return;
        }
        proxyState.checkValidObject(value);
        proxyState.getRow$realm().setLink(columnInfo.userIndex, ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getIndex());
    }

    @Override
    @SuppressWarnings("cast")
    public boolean realmGet$isMuted() {
        proxyState.getRealm$realm().checkIfValid();
        return (boolean) proxyState.getRow$realm().getBoolean(columnInfo.isMutedIndex);
    }

    @Override
    public void realmSet$isMuted(boolean value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setBoolean(columnInfo.isMutedIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setBoolean(columnInfo.isMutedIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$unReadCount() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.unReadCountIndex);
    }

    @Override
    public void realmSet$unReadCount(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.unReadCountIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.unReadCountIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$firstUnreadMessageId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.firstUnreadMessageIdIndex);
    }

    @Override
    public void realmSet$firstUnreadMessageId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.firstUnreadMessageIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.firstUnreadMessageIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.firstUnreadMessageIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.firstUnreadMessageIdIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$notificationId() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.notificationIdIndex);
    }

    @Override
    public void realmSet$notificationId(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.notificationIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.notificationIdIndex, value);
    }

    @Override
    public RealmList<org.wBHARATmeet.model.realms.Message> realmGet$unreadMessages() {
        proxyState.getRealm$realm().checkIfValid();
        // use the cached value if available
        if (unreadMessagesRealmList != null) {
            return unreadMessagesRealmList;
        } else {
            OsList osList = proxyState.getRow$realm().getModelList(columnInfo.unreadMessagesIndex);
            unreadMessagesRealmList = new RealmList<org.wBHARATmeet.model.realms.Message>(org.wBHARATmeet.model.realms.Message.class, osList, proxyState.getRealm$realm());
            return unreadMessagesRealmList;
        }
    }

    @Override
    public void realmSet$unreadMessages(RealmList<org.wBHARATmeet.model.realms.Message> value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("unreadMessages")) {
                return;
            }
            // if the list contains unmanaged RealmObjects, convert them to managed.
            if (value != null && !value.isManaged()) {
                final Realm realm = (Realm) proxyState.getRealm$realm();
                final RealmList<org.wBHARATmeet.model.realms.Message> original = value;
                value = new RealmList<org.wBHARATmeet.model.realms.Message>();
                for (org.wBHARATmeet.model.realms.Message item : original) {
                    if (item == null || RealmObject.isManaged(item)) {
                        value.add(item);
                    } else {
                        value.add(realm.copyToRealm(item));
                    }
                }
            }
        }

        proxyState.getRealm$realm().checkIfValid();
        OsList osList = proxyState.getRow$realm().getModelList(columnInfo.unreadMessagesIndex);
        // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
        if (value != null && value.size() == osList.size()) {
            int objects = value.size();
            for (int i = 0; i < objects; i++) {
                org.wBHARATmeet.model.realms.Message linkedObject = value.get(i);
                proxyState.checkValidObject(linkedObject);
                osList.setRow(i, ((RealmObjectProxy) linkedObject).realmGet$proxyState().getRow$realm().getIndex());
            }
        } else {
            osList.removeAll();
            if (value == null) {
                return;
            }
            int objects = value.size();
            for (int i = 0; i < objects; i++) {
                org.wBHARATmeet.model.realms.Message linkedObject = value.get(i);
                proxyState.checkValidObject(linkedObject);
                osList.addRow(((RealmObjectProxy) linkedObject).realmGet$proxyState().getRow$realm().getIndex());
            }
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("Chat", 9, 0);
        builder.addPersistedProperty("chatId", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedLinkProperty("lastMessage", RealmFieldType.OBJECT, "Message");
        builder.addPersistedProperty("lastMessageTimestamp", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedLinkProperty("user", RealmFieldType.OBJECT, "User");
        builder.addPersistedProperty("isMuted", RealmFieldType.BOOLEAN, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("unReadCount", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("firstUnreadMessageId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("notificationId", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedLinkProperty("unreadMessages", RealmFieldType.LIST, "Message");
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static ChatColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new ChatColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "Chat";
    }

    public static final class ClassNameHelper {
        public static final String INTERNAL_CLASS_NAME = "Chat";
    }

    @SuppressWarnings("cast")
    public static org.wBHARATmeet.model.realms.Chat createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = new ArrayList<String>(3);
        org.wBHARATmeet.model.realms.Chat obj = null;
        if (update) {
            Table table = realm.getTable(org.wBHARATmeet.model.realms.Chat.class);
            ChatColumnInfo columnInfo = (ChatColumnInfo) realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.Chat.class);
            long pkColumnIndex = columnInfo.chatIdIndex;
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("chatId")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("chatId"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.Chat.class), false, Collections.<String> emptyList());
                    obj = new io.realm.org_wBHARATmeet_model_realms_ChatRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("lastMessage")) {
                excludeFields.add("lastMessage");
            }
            if (json.has("user")) {
                excludeFields.add("user");
            }
            if (json.has("unreadMessages")) {
                excludeFields.add("unreadMessages");
            }
            if (json.has("chatId")) {
                if (json.isNull("chatId")) {
                    obj = (io.realm.org_wBHARATmeet_model_realms_ChatRealmProxy) realm.createObjectInternal(org.wBHARATmeet.model.realms.Chat.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.org_wBHARATmeet_model_realms_ChatRealmProxy) realm.createObjectInternal(org.wBHARATmeet.model.realms.Chat.class, json.getString("chatId"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'chatId'.");
            }
        }

        final org_wBHARATmeet_model_realms_ChatRealmProxyInterface objProxy = (org_wBHARATmeet_model_realms_ChatRealmProxyInterface) obj;
        if (json.has("lastMessage")) {
            if (json.isNull("lastMessage")) {
                objProxy.realmSet$lastMessage(null);
            } else {
                org.wBHARATmeet.model.realms.Message lastMessageObj = org_wBHARATmeet_model_realms_MessageRealmProxy.createOrUpdateUsingJsonObject(realm, json.getJSONObject("lastMessage"), update);
                objProxy.realmSet$lastMessage(lastMessageObj);
            }
        }
        if (json.has("lastMessageTimestamp")) {
            if (json.isNull("lastMessageTimestamp")) {
                objProxy.realmSet$lastMessageTimestamp(null);
            } else {
                objProxy.realmSet$lastMessageTimestamp((String) json.getString("lastMessageTimestamp"));
            }
        }
        if (json.has("user")) {
            if (json.isNull("user")) {
                objProxy.realmSet$user(null);
            } else {
                org.wBHARATmeet.model.realms.User userObj = org_wBHARATmeet_model_realms_UserRealmProxy.createOrUpdateUsingJsonObject(realm, json.getJSONObject("user"), update);
                objProxy.realmSet$user(userObj);
            }
        }
        if (json.has("isMuted")) {
            if (json.isNull("isMuted")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'isMuted' to null.");
            } else {
                objProxy.realmSet$isMuted((boolean) json.getBoolean("isMuted"));
            }
        }
        if (json.has("unReadCount")) {
            if (json.isNull("unReadCount")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'unReadCount' to null.");
            } else {
                objProxy.realmSet$unReadCount((int) json.getInt("unReadCount"));
            }
        }
        if (json.has("firstUnreadMessageId")) {
            if (json.isNull("firstUnreadMessageId")) {
                objProxy.realmSet$firstUnreadMessageId(null);
            } else {
                objProxy.realmSet$firstUnreadMessageId((String) json.getString("firstUnreadMessageId"));
            }
        }
        if (json.has("notificationId")) {
            if (json.isNull("notificationId")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'notificationId' to null.");
            } else {
                objProxy.realmSet$notificationId((int) json.getInt("notificationId"));
            }
        }
        if (json.has("unreadMessages")) {
            if (json.isNull("unreadMessages")) {
                objProxy.realmSet$unreadMessages(null);
            } else {
                objProxy.realmGet$unreadMessages().clear();
                JSONArray array = json.getJSONArray("unreadMessages");
                for (int i = 0; i < array.length(); i++) {
                    org.wBHARATmeet.model.realms.Message item = org_wBHARATmeet_model_realms_MessageRealmProxy.createOrUpdateUsingJsonObject(realm, array.getJSONObject(i), update);
                    objProxy.realmGet$unreadMessages().add(item);
                }
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static org.wBHARATmeet.model.realms.Chat createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final org.wBHARATmeet.model.realms.Chat obj = new org.wBHARATmeet.model.realms.Chat();
        final org_wBHARATmeet_model_realms_ChatRealmProxyInterface objProxy = (org_wBHARATmeet_model_realms_ChatRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("chatId")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$chatId((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$chatId(null);
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("lastMessage")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$lastMessage(null);
                } else {
                    org.wBHARATmeet.model.realms.Message lastMessageObj = org_wBHARATmeet_model_realms_MessageRealmProxy.createUsingJsonStream(realm, reader);
                    objProxy.realmSet$lastMessage(lastMessageObj);
                }
            } else if (name.equals("lastMessageTimestamp")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$lastMessageTimestamp((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$lastMessageTimestamp(null);
                }
            } else if (name.equals("user")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$user(null);
                } else {
                    org.wBHARATmeet.model.realms.User userObj = org_wBHARATmeet_model_realms_UserRealmProxy.createUsingJsonStream(realm, reader);
                    objProxy.realmSet$user(userObj);
                }
            } else if (name.equals("isMuted")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$isMuted((boolean) reader.nextBoolean());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'isMuted' to null.");
                }
            } else if (name.equals("unReadCount")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$unReadCount((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'unReadCount' to null.");
                }
            } else if (name.equals("firstUnreadMessageId")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$firstUnreadMessageId((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$firstUnreadMessageId(null);
                }
            } else if (name.equals("notificationId")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$notificationId((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'notificationId' to null.");
                }
            } else if (name.equals("unreadMessages")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$unreadMessages(null);
                } else {
                    objProxy.realmSet$unreadMessages(new RealmList<org.wBHARATmeet.model.realms.Message>());
                    reader.beginArray();
                    while (reader.hasNext()) {
                        org.wBHARATmeet.model.realms.Message item = org_wBHARATmeet_model_realms_MessageRealmProxy.createUsingJsonStream(realm, reader);
                        objProxy.realmGet$unreadMessages().add(item);
                    }
                    reader.endArray();
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'chatId'.");
        }
        return realm.copyToRealm(obj);
    }

    private static org_wBHARATmeet_model_realms_ChatRealmProxy newProxyInstance(BaseRealm realm, Row row) {
        // Ignore default values to avoid creating unexpected objects from RealmModel/RealmList fields
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        objectContext.set(realm, row, realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.Chat.class), false, Collections.<String>emptyList());
        io.realm.org_wBHARATmeet_model_realms_ChatRealmProxy obj = new io.realm.org_wBHARATmeet_model_realms_ChatRealmProxy();
        objectContext.clear();
        return obj;
    }

    public static org.wBHARATmeet.model.realms.Chat copyOrUpdate(Realm realm, ChatColumnInfo columnInfo, org.wBHARATmeet.model.realms.Chat object, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (org.wBHARATmeet.model.realms.Chat) cachedRealmObject;
        }

        org.wBHARATmeet.model.realms.Chat realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(org.wBHARATmeet.model.realms.Chat.class);
            long pkColumnIndex = columnInfo.chatIdIndex;
            String value = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$chatId();
            long rowIndex = Table.NO_MATCH;
            if (value == null) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, value);
            }
            if (rowIndex == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), columnInfo, false, Collections.<String> emptyList());
                    realmObject = new io.realm.org_wBHARATmeet_model_realms_ChatRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, columnInfo, realmObject, object, cache, flags) : copy(realm, columnInfo, object, update, cache, flags);
    }

    public static org.wBHARATmeet.model.realms.Chat copy(Realm realm, ChatColumnInfo columnInfo, org.wBHARATmeet.model.realms.Chat newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (org.wBHARATmeet.model.realms.Chat) cachedRealmObject;
        }

        org_wBHARATmeet_model_realms_ChatRealmProxyInterface realmObjectSource = (org_wBHARATmeet_model_realms_ChatRealmProxyInterface) newObject;

        Table table = realm.getTable(org.wBHARATmeet.model.realms.Chat.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, columnInfo.maxColumnIndexValue, flags);

        // Add all non-"object reference" fields
        builder.addString(columnInfo.chatIdIndex, realmObjectSource.realmGet$chatId());
        builder.addString(columnInfo.lastMessageTimestampIndex, realmObjectSource.realmGet$lastMessageTimestamp());
        builder.addBoolean(columnInfo.isMutedIndex, realmObjectSource.realmGet$isMuted());
        builder.addInteger(columnInfo.unReadCountIndex, realmObjectSource.realmGet$unReadCount());
        builder.addString(columnInfo.firstUnreadMessageIdIndex, realmObjectSource.realmGet$firstUnreadMessageId());
        builder.addInteger(columnInfo.notificationIdIndex, realmObjectSource.realmGet$notificationId());

        // Create the underlying object and cache it before setting any object/objectlist references
        // This will allow us to break any circular dependencies by using the object cache.
        Row row = builder.createNewObject();
        io.realm.org_wBHARATmeet_model_realms_ChatRealmProxy realmObjectCopy = newProxyInstance(realm, row);
        cache.put(newObject, realmObjectCopy);

        // Finally add all fields that reference other Realm Objects, either directly or through a list
        org.wBHARATmeet.model.realms.Message lastMessageObj = realmObjectSource.realmGet$lastMessage();
        if (lastMessageObj == null) {
            realmObjectCopy.realmSet$lastMessage(null);
        } else {
            org.wBHARATmeet.model.realms.Message cachelastMessage = (org.wBHARATmeet.model.realms.Message) cache.get(lastMessageObj);
            if (cachelastMessage != null) {
                realmObjectCopy.realmSet$lastMessage(cachelastMessage);
            } else {
                realmObjectCopy.realmSet$lastMessage(org_wBHARATmeet_model_realms_MessageRealmProxy.copyOrUpdate(realm, (org_wBHARATmeet_model_realms_MessageRealmProxy.MessageColumnInfo) realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.Message.class), lastMessageObj, update, cache, flags));
            }
        }

        org.wBHARATmeet.model.realms.User userObj = realmObjectSource.realmGet$user();
        if (userObj == null) {
            realmObjectCopy.realmSet$user(null);
        } else {
            org.wBHARATmeet.model.realms.User cacheuser = (org.wBHARATmeet.model.realms.User) cache.get(userObj);
            if (cacheuser != null) {
                realmObjectCopy.realmSet$user(cacheuser);
            } else {
                realmObjectCopy.realmSet$user(org_wBHARATmeet_model_realms_UserRealmProxy.copyOrUpdate(realm, (org_wBHARATmeet_model_realms_UserRealmProxy.UserColumnInfo) realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.User.class), userObj, update, cache, flags));
            }
        }

        RealmList<org.wBHARATmeet.model.realms.Message> unreadMessagesList = realmObjectSource.realmGet$unreadMessages();
        if (unreadMessagesList != null) {
            RealmList<org.wBHARATmeet.model.realms.Message> unreadMessagesRealmList = realmObjectCopy.realmGet$unreadMessages();
            unreadMessagesRealmList.clear();
            for (int i = 0; i < unreadMessagesList.size(); i++) {
                org.wBHARATmeet.model.realms.Message unreadMessagesItem = unreadMessagesList.get(i);
                org.wBHARATmeet.model.realms.Message cacheunreadMessages = (org.wBHARATmeet.model.realms.Message) cache.get(unreadMessagesItem);
                if (cacheunreadMessages != null) {
                    unreadMessagesRealmList.add(cacheunreadMessages);
                } else {
                    unreadMessagesRealmList.add(org_wBHARATmeet_model_realms_MessageRealmProxy.copyOrUpdate(realm, (org_wBHARATmeet_model_realms_MessageRealmProxy.MessageColumnInfo) realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.Message.class), unreadMessagesItem, update, cache, flags));
                }
            }
        }

        return realmObjectCopy;
    }

    public static long insert(Realm realm, org.wBHARATmeet.model.realms.Chat object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(org.wBHARATmeet.model.realms.Chat.class);
        long tableNativePtr = table.getNativePtr();
        ChatColumnInfo columnInfo = (ChatColumnInfo) realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.Chat.class);
        long pkColumnIndex = columnInfo.chatIdIndex;
        String primaryKeyValue = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$chatId();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);

        org.wBHARATmeet.model.realms.Message lastMessageObj = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$lastMessage();
        if (lastMessageObj != null) {
            Long cachelastMessage = cache.get(lastMessageObj);
            if (cachelastMessage == null) {
                cachelastMessage = org_wBHARATmeet_model_realms_MessageRealmProxy.insert(realm, lastMessageObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.lastMessageIndex, rowIndex, cachelastMessage, false);
        }
        String realmGet$lastMessageTimestamp = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$lastMessageTimestamp();
        if (realmGet$lastMessageTimestamp != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.lastMessageTimestampIndex, rowIndex, realmGet$lastMessageTimestamp, false);
        }

        org.wBHARATmeet.model.realms.User userObj = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$user();
        if (userObj != null) {
            Long cacheuser = cache.get(userObj);
            if (cacheuser == null) {
                cacheuser = org_wBHARATmeet_model_realms_UserRealmProxy.insert(realm, userObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.userIndex, rowIndex, cacheuser, false);
        }
        Table.nativeSetBoolean(tableNativePtr, columnInfo.isMutedIndex, rowIndex, ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$isMuted(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.unReadCountIndex, rowIndex, ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$unReadCount(), false);
        String realmGet$firstUnreadMessageId = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$firstUnreadMessageId();
        if (realmGet$firstUnreadMessageId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.firstUnreadMessageIdIndex, rowIndex, realmGet$firstUnreadMessageId, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.notificationIdIndex, rowIndex, ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$notificationId(), false);

        RealmList<org.wBHARATmeet.model.realms.Message> unreadMessagesList = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$unreadMessages();
        if (unreadMessagesList != null) {
            OsList unreadMessagesOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.unreadMessagesIndex);
            for (org.wBHARATmeet.model.realms.Message unreadMessagesItem : unreadMessagesList) {
                Long cacheItemIndexunreadMessages = cache.get(unreadMessagesItem);
                if (cacheItemIndexunreadMessages == null) {
                    cacheItemIndexunreadMessages = org_wBHARATmeet_model_realms_MessageRealmProxy.insert(realm, unreadMessagesItem, cache);
                }
                unreadMessagesOsList.addRow(cacheItemIndexunreadMessages);
            }
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(org.wBHARATmeet.model.realms.Chat.class);
        long tableNativePtr = table.getNativePtr();
        ChatColumnInfo columnInfo = (ChatColumnInfo) realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.Chat.class);
        long pkColumnIndex = columnInfo.chatIdIndex;
        org.wBHARATmeet.model.realms.Chat object = null;
        while (objects.hasNext()) {
            object = (org.wBHARATmeet.model.realms.Chat) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            String primaryKeyValue = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$chatId();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);

            org.wBHARATmeet.model.realms.Message lastMessageObj = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$lastMessage();
            if (lastMessageObj != null) {
                Long cachelastMessage = cache.get(lastMessageObj);
                if (cachelastMessage == null) {
                    cachelastMessage = org_wBHARATmeet_model_realms_MessageRealmProxy.insert(realm, lastMessageObj, cache);
                }
                table.setLink(columnInfo.lastMessageIndex, rowIndex, cachelastMessage, false);
            }
            String realmGet$lastMessageTimestamp = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$lastMessageTimestamp();
            if (realmGet$lastMessageTimestamp != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.lastMessageTimestampIndex, rowIndex, realmGet$lastMessageTimestamp, false);
            }

            org.wBHARATmeet.model.realms.User userObj = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$user();
            if (userObj != null) {
                Long cacheuser = cache.get(userObj);
                if (cacheuser == null) {
                    cacheuser = org_wBHARATmeet_model_realms_UserRealmProxy.insert(realm, userObj, cache);
                }
                table.setLink(columnInfo.userIndex, rowIndex, cacheuser, false);
            }
            Table.nativeSetBoolean(tableNativePtr, columnInfo.isMutedIndex, rowIndex, ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$isMuted(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.unReadCountIndex, rowIndex, ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$unReadCount(), false);
            String realmGet$firstUnreadMessageId = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$firstUnreadMessageId();
            if (realmGet$firstUnreadMessageId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.firstUnreadMessageIdIndex, rowIndex, realmGet$firstUnreadMessageId, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.notificationIdIndex, rowIndex, ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$notificationId(), false);

            RealmList<org.wBHARATmeet.model.realms.Message> unreadMessagesList = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$unreadMessages();
            if (unreadMessagesList != null) {
                OsList unreadMessagesOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.unreadMessagesIndex);
                for (org.wBHARATmeet.model.realms.Message unreadMessagesItem : unreadMessagesList) {
                    Long cacheItemIndexunreadMessages = cache.get(unreadMessagesItem);
                    if (cacheItemIndexunreadMessages == null) {
                        cacheItemIndexunreadMessages = org_wBHARATmeet_model_realms_MessageRealmProxy.insert(realm, unreadMessagesItem, cache);
                    }
                    unreadMessagesOsList.addRow(cacheItemIndexunreadMessages);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, org.wBHARATmeet.model.realms.Chat object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(org.wBHARATmeet.model.realms.Chat.class);
        long tableNativePtr = table.getNativePtr();
        ChatColumnInfo columnInfo = (ChatColumnInfo) realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.Chat.class);
        long pkColumnIndex = columnInfo.chatIdIndex;
        String primaryKeyValue = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$chatId();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
        }
        cache.put(object, rowIndex);

        org.wBHARATmeet.model.realms.Message lastMessageObj = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$lastMessage();
        if (lastMessageObj != null) {
            Long cachelastMessage = cache.get(lastMessageObj);
            if (cachelastMessage == null) {
                cachelastMessage = org_wBHARATmeet_model_realms_MessageRealmProxy.insertOrUpdate(realm, lastMessageObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.lastMessageIndex, rowIndex, cachelastMessage, false);
        } else {
            Table.nativeNullifyLink(tableNativePtr, columnInfo.lastMessageIndex, rowIndex);
        }
        String realmGet$lastMessageTimestamp = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$lastMessageTimestamp();
        if (realmGet$lastMessageTimestamp != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.lastMessageTimestampIndex, rowIndex, realmGet$lastMessageTimestamp, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.lastMessageTimestampIndex, rowIndex, false);
        }

        org.wBHARATmeet.model.realms.User userObj = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$user();
        if (userObj != null) {
            Long cacheuser = cache.get(userObj);
            if (cacheuser == null) {
                cacheuser = org_wBHARATmeet_model_realms_UserRealmProxy.insertOrUpdate(realm, userObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.userIndex, rowIndex, cacheuser, false);
        } else {
            Table.nativeNullifyLink(tableNativePtr, columnInfo.userIndex, rowIndex);
        }
        Table.nativeSetBoolean(tableNativePtr, columnInfo.isMutedIndex, rowIndex, ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$isMuted(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.unReadCountIndex, rowIndex, ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$unReadCount(), false);
        String realmGet$firstUnreadMessageId = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$firstUnreadMessageId();
        if (realmGet$firstUnreadMessageId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.firstUnreadMessageIdIndex, rowIndex, realmGet$firstUnreadMessageId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.firstUnreadMessageIdIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.notificationIdIndex, rowIndex, ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$notificationId(), false);

        OsList unreadMessagesOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.unreadMessagesIndex);
        RealmList<org.wBHARATmeet.model.realms.Message> unreadMessagesList = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$unreadMessages();
        if (unreadMessagesList != null && unreadMessagesList.size() == unreadMessagesOsList.size()) {
            // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
            int objects = unreadMessagesList.size();
            for (int i = 0; i < objects; i++) {
                org.wBHARATmeet.model.realms.Message unreadMessagesItem = unreadMessagesList.get(i);
                Long cacheItemIndexunreadMessages = cache.get(unreadMessagesItem);
                if (cacheItemIndexunreadMessages == null) {
                    cacheItemIndexunreadMessages = org_wBHARATmeet_model_realms_MessageRealmProxy.insertOrUpdate(realm, unreadMessagesItem, cache);
                }
                unreadMessagesOsList.setRow(i, cacheItemIndexunreadMessages);
            }
        } else {
            unreadMessagesOsList.removeAll();
            if (unreadMessagesList != null) {
                for (org.wBHARATmeet.model.realms.Message unreadMessagesItem : unreadMessagesList) {
                    Long cacheItemIndexunreadMessages = cache.get(unreadMessagesItem);
                    if (cacheItemIndexunreadMessages == null) {
                        cacheItemIndexunreadMessages = org_wBHARATmeet_model_realms_MessageRealmProxy.insertOrUpdate(realm, unreadMessagesItem, cache);
                    }
                    unreadMessagesOsList.addRow(cacheItemIndexunreadMessages);
                }
            }
        }

        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(org.wBHARATmeet.model.realms.Chat.class);
        long tableNativePtr = table.getNativePtr();
        ChatColumnInfo columnInfo = (ChatColumnInfo) realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.Chat.class);
        long pkColumnIndex = columnInfo.chatIdIndex;
        org.wBHARATmeet.model.realms.Chat object = null;
        while (objects.hasNext()) {
            object = (org.wBHARATmeet.model.realms.Chat) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            String primaryKeyValue = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$chatId();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
            }
            cache.put(object, rowIndex);

            org.wBHARATmeet.model.realms.Message lastMessageObj = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$lastMessage();
            if (lastMessageObj != null) {
                Long cachelastMessage = cache.get(lastMessageObj);
                if (cachelastMessage == null) {
                    cachelastMessage = org_wBHARATmeet_model_realms_MessageRealmProxy.insertOrUpdate(realm, lastMessageObj, cache);
                }
                Table.nativeSetLink(tableNativePtr, columnInfo.lastMessageIndex, rowIndex, cachelastMessage, false);
            } else {
                Table.nativeNullifyLink(tableNativePtr, columnInfo.lastMessageIndex, rowIndex);
            }
            String realmGet$lastMessageTimestamp = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$lastMessageTimestamp();
            if (realmGet$lastMessageTimestamp != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.lastMessageTimestampIndex, rowIndex, realmGet$lastMessageTimestamp, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.lastMessageTimestampIndex, rowIndex, false);
            }

            org.wBHARATmeet.model.realms.User userObj = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$user();
            if (userObj != null) {
                Long cacheuser = cache.get(userObj);
                if (cacheuser == null) {
                    cacheuser = org_wBHARATmeet_model_realms_UserRealmProxy.insertOrUpdate(realm, userObj, cache);
                }
                Table.nativeSetLink(tableNativePtr, columnInfo.userIndex, rowIndex, cacheuser, false);
            } else {
                Table.nativeNullifyLink(tableNativePtr, columnInfo.userIndex, rowIndex);
            }
            Table.nativeSetBoolean(tableNativePtr, columnInfo.isMutedIndex, rowIndex, ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$isMuted(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.unReadCountIndex, rowIndex, ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$unReadCount(), false);
            String realmGet$firstUnreadMessageId = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$firstUnreadMessageId();
            if (realmGet$firstUnreadMessageId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.firstUnreadMessageIdIndex, rowIndex, realmGet$firstUnreadMessageId, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.firstUnreadMessageIdIndex, rowIndex, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.notificationIdIndex, rowIndex, ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$notificationId(), false);

            OsList unreadMessagesOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.unreadMessagesIndex);
            RealmList<org.wBHARATmeet.model.realms.Message> unreadMessagesList = ((org_wBHARATmeet_model_realms_ChatRealmProxyInterface) object).realmGet$unreadMessages();
            if (unreadMessagesList != null && unreadMessagesList.size() == unreadMessagesOsList.size()) {
                // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
                int objectCount = unreadMessagesList.size();
                for (int i = 0; i < objectCount; i++) {
                    org.wBHARATmeet.model.realms.Message unreadMessagesItem = unreadMessagesList.get(i);
                    Long cacheItemIndexunreadMessages = cache.get(unreadMessagesItem);
                    if (cacheItemIndexunreadMessages == null) {
                        cacheItemIndexunreadMessages = org_wBHARATmeet_model_realms_MessageRealmProxy.insertOrUpdate(realm, unreadMessagesItem, cache);
                    }
                    unreadMessagesOsList.setRow(i, cacheItemIndexunreadMessages);
                }
            } else {
                unreadMessagesOsList.removeAll();
                if (unreadMessagesList != null) {
                    for (org.wBHARATmeet.model.realms.Message unreadMessagesItem : unreadMessagesList) {
                        Long cacheItemIndexunreadMessages = cache.get(unreadMessagesItem);
                        if (cacheItemIndexunreadMessages == null) {
                            cacheItemIndexunreadMessages = org_wBHARATmeet_model_realms_MessageRealmProxy.insertOrUpdate(realm, unreadMessagesItem, cache);
                        }
                        unreadMessagesOsList.addRow(cacheItemIndexunreadMessages);
                    }
                }
            }

        }
    }

    public static org.wBHARATmeet.model.realms.Chat createDetachedCopy(org.wBHARATmeet.model.realms.Chat realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        org.wBHARATmeet.model.realms.Chat unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new org.wBHARATmeet.model.realms.Chat();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (org.wBHARATmeet.model.realms.Chat) cachedObject.object;
            }
            unmanagedObject = (org.wBHARATmeet.model.realms.Chat) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        org_wBHARATmeet_model_realms_ChatRealmProxyInterface unmanagedCopy = (org_wBHARATmeet_model_realms_ChatRealmProxyInterface) unmanagedObject;
        org_wBHARATmeet_model_realms_ChatRealmProxyInterface realmSource = (org_wBHARATmeet_model_realms_ChatRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$chatId(realmSource.realmGet$chatId());

        // Deep copy of lastMessage
        unmanagedCopy.realmSet$lastMessage(org_wBHARATmeet_model_realms_MessageRealmProxy.createDetachedCopy(realmSource.realmGet$lastMessage(), currentDepth + 1, maxDepth, cache));
        unmanagedCopy.realmSet$lastMessageTimestamp(realmSource.realmGet$lastMessageTimestamp());

        // Deep copy of user
        unmanagedCopy.realmSet$user(org_wBHARATmeet_model_realms_UserRealmProxy.createDetachedCopy(realmSource.realmGet$user(), currentDepth + 1, maxDepth, cache));
        unmanagedCopy.realmSet$isMuted(realmSource.realmGet$isMuted());
        unmanagedCopy.realmSet$unReadCount(realmSource.realmGet$unReadCount());
        unmanagedCopy.realmSet$firstUnreadMessageId(realmSource.realmGet$firstUnreadMessageId());
        unmanagedCopy.realmSet$notificationId(realmSource.realmGet$notificationId());

        // Deep copy of unreadMessages
        if (currentDepth == maxDepth) {
            unmanagedCopy.realmSet$unreadMessages(null);
        } else {
            RealmList<org.wBHARATmeet.model.realms.Message> managedunreadMessagesList = realmSource.realmGet$unreadMessages();
            RealmList<org.wBHARATmeet.model.realms.Message> unmanagedunreadMessagesList = new RealmList<org.wBHARATmeet.model.realms.Message>();
            unmanagedCopy.realmSet$unreadMessages(unmanagedunreadMessagesList);
            int nextDepth = currentDepth + 1;
            int size = managedunreadMessagesList.size();
            for (int i = 0; i < size; i++) {
                org.wBHARATmeet.model.realms.Message item = org_wBHARATmeet_model_realms_MessageRealmProxy.createDetachedCopy(managedunreadMessagesList.get(i), nextDepth, maxDepth, cache);
                unmanagedunreadMessagesList.add(item);
            }
        }

        return unmanagedObject;
    }

    static org.wBHARATmeet.model.realms.Chat update(Realm realm, ChatColumnInfo columnInfo, org.wBHARATmeet.model.realms.Chat realmObject, org.wBHARATmeet.model.realms.Chat newObject, Map<RealmModel, RealmObjectProxy> cache, Set<ImportFlag> flags) {
        org_wBHARATmeet_model_realms_ChatRealmProxyInterface realmObjectTarget = (org_wBHARATmeet_model_realms_ChatRealmProxyInterface) realmObject;
        org_wBHARATmeet_model_realms_ChatRealmProxyInterface realmObjectSource = (org_wBHARATmeet_model_realms_ChatRealmProxyInterface) newObject;
        Table table = realm.getTable(org.wBHARATmeet.model.realms.Chat.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, columnInfo.maxColumnIndexValue, flags);
        builder.addString(columnInfo.chatIdIndex, realmObjectSource.realmGet$chatId());

        org.wBHARATmeet.model.realms.Message lastMessageObj = realmObjectSource.realmGet$lastMessage();
        if (lastMessageObj == null) {
            builder.addNull(columnInfo.lastMessageIndex);
        } else {
            org.wBHARATmeet.model.realms.Message cachelastMessage = (org.wBHARATmeet.model.realms.Message) cache.get(lastMessageObj);
            if (cachelastMessage != null) {
                builder.addObject(columnInfo.lastMessageIndex, cachelastMessage);
            } else {
                builder.addObject(columnInfo.lastMessageIndex, org_wBHARATmeet_model_realms_MessageRealmProxy.copyOrUpdate(realm, (org_wBHARATmeet_model_realms_MessageRealmProxy.MessageColumnInfo) realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.Message.class), lastMessageObj, true, cache, flags));
            }
        }
        builder.addString(columnInfo.lastMessageTimestampIndex, realmObjectSource.realmGet$lastMessageTimestamp());

        org.wBHARATmeet.model.realms.User userObj = realmObjectSource.realmGet$user();
        if (userObj == null) {
            builder.addNull(columnInfo.userIndex);
        } else {
            org.wBHARATmeet.model.realms.User cacheuser = (org.wBHARATmeet.model.realms.User) cache.get(userObj);
            if (cacheuser != null) {
                builder.addObject(columnInfo.userIndex, cacheuser);
            } else {
                builder.addObject(columnInfo.userIndex, org_wBHARATmeet_model_realms_UserRealmProxy.copyOrUpdate(realm, (org_wBHARATmeet_model_realms_UserRealmProxy.UserColumnInfo) realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.User.class), userObj, true, cache, flags));
            }
        }
        builder.addBoolean(columnInfo.isMutedIndex, realmObjectSource.realmGet$isMuted());
        builder.addInteger(columnInfo.unReadCountIndex, realmObjectSource.realmGet$unReadCount());
        builder.addString(columnInfo.firstUnreadMessageIdIndex, realmObjectSource.realmGet$firstUnreadMessageId());
        builder.addInteger(columnInfo.notificationIdIndex, realmObjectSource.realmGet$notificationId());

        RealmList<org.wBHARATmeet.model.realms.Message> unreadMessagesList = realmObjectSource.realmGet$unreadMessages();
        if (unreadMessagesList != null) {
            RealmList<org.wBHARATmeet.model.realms.Message> unreadMessagesManagedCopy = new RealmList<org.wBHARATmeet.model.realms.Message>();
            for (int i = 0; i < unreadMessagesList.size(); i++) {
                org.wBHARATmeet.model.realms.Message unreadMessagesItem = unreadMessagesList.get(i);
                org.wBHARATmeet.model.realms.Message cacheunreadMessages = (org.wBHARATmeet.model.realms.Message) cache.get(unreadMessagesItem);
                if (cacheunreadMessages != null) {
                    unreadMessagesManagedCopy.add(cacheunreadMessages);
                } else {
                    unreadMessagesManagedCopy.add(org_wBHARATmeet_model_realms_MessageRealmProxy.copyOrUpdate(realm, (org_wBHARATmeet_model_realms_MessageRealmProxy.MessageColumnInfo) realm.getSchema().getColumnInfo(org.wBHARATmeet.model.realms.Message.class), unreadMessagesItem, true, cache, flags));
                }
            }
            builder.addObjectList(columnInfo.unreadMessagesIndex, unreadMessagesManagedCopy);
        } else {
            builder.addObjectList(columnInfo.unreadMessagesIndex, new RealmList<org.wBHARATmeet.model.realms.Message>());
        }

        builder.updateExistingObject();
        return realmObject;
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

}
