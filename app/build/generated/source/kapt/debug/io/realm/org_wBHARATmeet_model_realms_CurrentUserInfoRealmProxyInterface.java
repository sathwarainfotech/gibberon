package io.realm;


public interface org_wBHARATmeet_model_realms_CurrentUserInfoRealmProxyInterface {
    public String realmGet$uid();
    public void realmSet$uid(String value);
    public String realmGet$phone();
    public void realmSet$phone(String value);
}
