package io.realm;


public interface org_wBHARATmeet_model_realms_DeletedMessageRealmProxyInterface {
    public String realmGet$messageId();
    public void realmSet$messageId(String value);
}
