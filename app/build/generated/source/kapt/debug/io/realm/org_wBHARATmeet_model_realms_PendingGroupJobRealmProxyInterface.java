package io.realm;


public interface org_wBHARATmeet_model_realms_PendingGroupJobRealmProxyInterface {
    public String realmGet$groupId();
    public void realmSet$groupId(String value);
    public int realmGet$type();
    public void realmSet$type(int value);
    public org.wBHARATmeet.model.realms.GroupEvent realmGet$groupEvent();
    public void realmSet$groupEvent(org.wBHARATmeet.model.realms.GroupEvent value);
}
