package io.realm;


public interface org_wBHARATmeet_model_realms_RealmContactRealmProxyInterface {
    public String realmGet$name();
    public void realmSet$name(String value);
    public RealmList<org.wBHARATmeet.model.realms.PhoneNumber> realmGet$realmList();
    public void realmSet$realmList(RealmList<org.wBHARATmeet.model.realms.PhoneNumber> value);
}
