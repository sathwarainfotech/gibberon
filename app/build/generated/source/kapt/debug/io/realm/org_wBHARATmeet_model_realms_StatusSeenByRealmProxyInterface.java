package io.realm;


public interface org_wBHARATmeet_model_realms_StatusSeenByRealmProxyInterface {
    public org.wBHARATmeet.model.realms.User realmGet$user();
    public void realmSet$user(org.wBHARATmeet.model.realms.User value);
    public long realmGet$seenAt();
    public void realmSet$seenAt(long value);
}
