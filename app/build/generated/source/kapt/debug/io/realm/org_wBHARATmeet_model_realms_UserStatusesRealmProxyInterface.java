package io.realm;


public interface org_wBHARATmeet_model_realms_UserStatusesRealmProxyInterface {
    public String realmGet$userId();
    public void realmSet$userId(String value);
    public long realmGet$lastStatusTimestamp();
    public void realmSet$lastStatusTimestamp(long value);
    public org.wBHARATmeet.model.realms.User realmGet$user();
    public void realmSet$user(org.wBHARATmeet.model.realms.User value);
    public RealmList<org.wBHARATmeet.model.realms.Status> realmGet$statuses();
    public void realmSet$statuses(RealmList<org.wBHARATmeet.model.realms.Status> value);
    public boolean realmGet$areAllSeen();
    public void realmSet$areAllSeen(boolean value);
}
