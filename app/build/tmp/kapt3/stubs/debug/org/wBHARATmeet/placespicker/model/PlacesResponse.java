package org.wBHARATmeet.placespicker.model;

import java.lang.System;

@androidx.annotation.Keep()
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0087\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0016\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lorg/wBHARATmeet/placespicker/model/PlacesResponse;", "", "meta", "Lorg/wBHARATmeet/placespicker/model/Meta;", "response", "Lorg/wBHARATmeet/placespicker/model/Response;", "(Lorg/wBHARATmeet/placespicker/model/Meta;Lorg/wBHARATmeet/placespicker/model/Response;)V", "getMeta", "()Lorg/wBHARATmeet/placespicker/model/Meta;", "getResponse", "()Lorg/wBHARATmeet/placespicker/model/Response;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_debug"})
public final class PlacesResponse {
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "meta")
    private final org.wBHARATmeet.placespicker.model.Meta meta = null;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "response")
    private final org.wBHARATmeet.placespicker.model.Response response = null;
    
    @org.jetbrains.annotations.NotNull()
    public final org.wBHARATmeet.placespicker.model.Meta getMeta() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.wBHARATmeet.placespicker.model.Response getResponse() {
        return null;
    }
    
    public PlacesResponse(@org.jetbrains.annotations.NotNull()
    org.wBHARATmeet.placespicker.model.Meta meta, @org.jetbrains.annotations.NotNull()
    org.wBHARATmeet.placespicker.model.Response response) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.wBHARATmeet.placespicker.model.Meta component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.wBHARATmeet.placespicker.model.Response component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.wBHARATmeet.placespicker.model.PlacesResponse copy(@org.jetbrains.annotations.NotNull()
    org.wBHARATmeet.placespicker.model.Meta meta, @org.jetbrains.annotations.NotNull()
    org.wBHARATmeet.placespicker.model.Response response) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}